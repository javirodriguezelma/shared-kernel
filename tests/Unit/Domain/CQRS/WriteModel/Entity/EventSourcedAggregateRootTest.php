<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\CQRS\WriteModel\Entity;

use PHPUnit\Framework\TestCase;
use StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity\Snapshot;
use StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent\IdStub;

class EventSourcedAggregateRootTest extends TestCase
{
    /**
     * @test
     */
    public function checkAnAggregateItsReconstituted()
    {
        // Arrange
        $id = IdStub::generate();
        $aggregateRoot = new EventSourcedAggregateRootStub($id, 'foo', 'bar');
        $eventStream = $aggregateRoot->pullEventStream();
        $reconstitutedAggregateRootReflection = new \ReflectionClass(EventSourcedAggregateRootStub::class);
        /** @var EventSourcedAggregateRootStub $reconstitutedAggregateRoot */
        $reconstitutedAggregateRoot = $reconstitutedAggregateRootReflection->newInstanceWithoutConstructor();

        // Act
        $reconstitutedAggregateRoot->reconstitute($eventStream);

        // Assert
        $this->assertEquals($id, $reconstitutedAggregateRoot->getId());
        $this->assertEquals('foo', $reconstitutedAggregateRoot->getFoo());
        $this->assertEquals('bar', $reconstitutedAggregateRoot->getBar());
    }

    /**
     * @test
     */
    public function checkAnAggregateItsReconstitutedWithSnapshot()
    {
        // Arrange
        $id = IdStub::generate();
        $aggregateRoot = new EventSourcedAggregateRootStub($id, 'foo_new', 'bar_new');
        $snapshot = new Snapshot([
            'foo' => 'foo_old',
            'bar' => 'bar_old'
        ]);
        $eventStream = $aggregateRoot->pullEventStream();
        $reconstitutedAggregateRootReflection = new \ReflectionClass(EventSourcedAggregateRootStub::class);
        /** @var EventSourcedAggregateRootStub $reconstitutedAggregateRoot */
        $reconstitutedAggregateRoot = $reconstitutedAggregateRootReflection->newInstanceWithoutConstructor();

        // Act
        $reconstitutedAggregateRoot->reconstitute($eventStream, $snapshot);

        // Assert
        $this->assertEquals($id, $reconstitutedAggregateRoot->getId());
        $this->assertEquals('foo_new', $reconstitutedAggregateRoot->getFoo());
        $this->assertEquals('bar_new', $reconstitutedAggregateRoot->getBar());
    }
}
