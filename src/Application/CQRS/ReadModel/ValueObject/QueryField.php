<?php

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject;

class QueryField
{
    const FLOAT_TYPE = 'float';
    const INT_TYPE = 'int';
    const STRING_TYPE = 'string';
    const BOOL_TYPE = 'bool';
    const ARRAY_STRING_TYPE = 'array_string';
    const ARRAY_INT_TYPE = 'array_int';
    const ARRAY_BOOL_TYPE = 'array_bool';
    const ARRAY_FLOAT_TYPE = 'array_float';

    const ALLOWED_TYPES = [
        self::FLOAT_TYPE,
        self::INT_TYPE,
        self::STRING_TYPE,
        self::BOOL_TYPE,
        self::ARRAY_STRING_TYPE,
        self::ARRAY_INT_TYPE,
        self::ARRAY_BOOL_TYPE,
        self::ARRAY_FLOAT_TYPE
    ];

    const FILTER_TYPES = [
        self::FLOAT_TYPE => FILTER_VALIDATE_FLOAT,
        self::INT_TYPE => FILTER_VALIDATE_INT,
        self::STRING_TYPE => FILTER_DEFAULT,
        self::BOOL_TYPE => FILTER_VALIDATE_BOOLEAN,
        self::ARRAY_STRING_TYPE => FILTER_DEFAULT,
        self::ARRAY_INT_TYPE => FILTER_VALIDATE_INT,
        self::ARRAY_BOOL_TYPE => FILTER_VALIDATE_BOOLEAN,
        self::ARRAY_FLOAT_TYPE => FILTER_VALIDATE_FLOAT
    ];

    private const FILTER_FUNCTION_TYPES = [
        self::FLOAT_TYPE => 'filter_var',
        self::INT_TYPE => 'filter_var',
        self::STRING_TYPE => 'filter_var',
        self::BOOL_TYPE => 'filter_var',
        self::ARRAY_STRING_TYPE => 'filter_var_array',
        self::ARRAY_INT_TYPE => 'filter_var_array',
        self::ARRAY_BOOL_TYPE => 'filter_var_array',
        self::ARRAY_FLOAT_TYPE => 'filter_var_array'
    ];

    private $name;
    private $type;

    public function __construct(string $name, string $type)
    {
        $this->checkTypeIsValid($type);
        $this->name = $name;
        $this->type = $type;
    }

    private function checkTypeIsValid(string $type): void
    {
        if (!in_array($type, self::ALLOWED_TYPES)) {
            throw new \InvalidArgumentException("$type is not a valid PHP type");
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function filterValue($value)
    {
        return call_user_func(QueryField::FILTER_FUNCTION_TYPES[$this->getType()],
            $value,
            QueryField::FILTER_TYPES[$this->getType()]
        );
    }
}
