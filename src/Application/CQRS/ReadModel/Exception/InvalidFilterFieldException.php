<?php

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\Exception;

use StraTDeS\SharedKernel\Application\Exception\BadRequestException;

class InvalidFilterFieldException extends BadRequestException
{

}