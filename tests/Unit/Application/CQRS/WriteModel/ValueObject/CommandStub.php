<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Application\CQRS\WriteModel\ValueObject;

use StraTDeS\SharedKernel\Application\CQRS\WriteModel\ValueObject\Command;

class CommandStub extends Command
{

}